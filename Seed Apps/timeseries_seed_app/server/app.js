"use strict";

const path = require("path");
const express = require("express");
const request = require("request");
const sampleData = require("./rawData.json");
// Used for session cookie
const cookieParser = require("cookie-parser");
const bodyParser = require("body-parser");
const moment = require("moment");
// Simple in-memory session is used here. use connect-redis for production!!
const session = require("express-session");
// Used to send gziped files
const compression = require("compression");
// Used when requesting data from real services.
const proxy = require("./proxy");
// Get config serverConfig from local file or VCAPS environment variable in the cloud
const serverConfig = require("./server-config");
// Configure passport for authentication with UAA
const passportConfig = require("./passport-config");
// Only used if you have configured properties for UAA
let passport;
let mainAuthenticate;

// Express server
const app = express();

// App configuration
const config = serverConfig.getServerConfig();


// Constants
const HTTP_INTERNAL_ERROR_STATUS = 500;

function getJson(url, method, reqBody) {
    return request({
            url: url,
            headers: {
                "content-type": "application/json",
            },
            method: method,
            json: reqBody
        },
        function (err, response, body) {
            if (!err && body) {
                return body;
            } else {
                errObj = { response: response, err: err };
                console.log(errObj);
                return JSON.parse(errObj);
            }
        });
}

/**********************************************************************
 SETTING UP EXPRESS SERVER
 ***********************************************************************/

app.set("trust proxy", 1);
app.use(cookieParser("predixsample"));
app.use(compression());

app.use(session({
    secret: "predixsample",
    name: "cookie_name",
    proxy: true,
    resave: true,
    saveUninitialized: true
}));

if (config.uaaIsConfigured) {
    passport = passportConfig.configurePassportStrategy();
    app.use(passport.initialize());
    // Also use passport.session() middleware, to support persistent login sessions (recommended).
    app.use(passport.session());

    mainAuthenticate = function (options) {
        return passportConfig.authenticate(options);
    };
} else {
    mainAuthenticate = function () {
        return function (req, res, next) {
            next();
        };
    };
}

// Initializing application modules
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

app.listen(config.port, function () {
    console.log("Server is listening at: " + config.appURL);
});

/****************************************************************************
 SET UP EXPRESS ROUTES
 *****************************************************************************/

if (config.uaaIsConfigured) {
    // Login route redirect to predix uaa login page
    app.get("/login", passport.authenticate("predix", {scope: ""}));

    // Callback route redirects to secure route after login
    app.get("/callback", function (req, res, next) {
        const successRedirect = req.session[config.callbackProp] || "/";

        req.session[config.callbackProp] = null;
        passport.authenticate("predix", {
            successRedirect: successRedirect,
            failureRedirect: "/login"
        })(req, res, next);
    });

    // Logout route
    app.get("/logout", function (req, res) {
        req.session.destroy();
        req.logout();
        res.redirect(config.uaaURL + "/logout?redirect=" + config.appURL);
    });
}

// Secured route to access Predix services or backend microservices
app.use("/api", mainAuthenticate({noRedirect: true}), proxy.router);

app.use("/json", function (req, res) {
    // Return JSON timeseries data
    let dataObj = [];

    // Get data set for date range

    // let trafficData = getJson("https://t14-engine.run.aws-usw02-pr.ice.predix.io/api/summary/123", "GET", {});

    let trafficData = sampleData;
    trafficData.data.forEach(function(element) {
        if(element.direction === 0) {
            let obj = { timestamp: 0, numVehicles: 0, avgSpeed: 0, lightflip: 0, emissions: 0};
            obj.timestamp = moment(element.timestamp).valueOf;
            obj.numVehicles = element.vehicle_count || 0;
            obj.avgSpeed = element.speed || 0;
            obj.lightflip = element.light_change || 0;
             // Get carbon emissions
            if (element.speed === 0) {
                let idleEmissions = getJson("https://exc-carbon.run.aws-usw02-pr.ice.predix.io/idleEmissions", "POST",
                { vehicleMpg: 20, idleTime: 5 });
                console.log("idleEmissions", JSON.stringify(idleEmissions));
                element.emissions = element.vehicle_count * idleEmissions.emissions;

            } else if (element.speed > 0 && element.light_change) {
                let accelEmissions = getJson("https://exc-carbon.run.aws-usw02-pr.ice.predix.io/accelEmissions", "POST",
                    { vehicleMpg: 20, speedLimit: element.speed, accelerationRate: element.speed / 5 });
                element.emissions = element.vehicle_count * accelEmissions.emissions;

            } else {
                let cruiseEmissions = getJson("https://exc-carbon.run.aws-usw02-pr.ice.predix.io/cruiseEmissions", "POST",
                    { vehicleMpg: 20, speedLimit: element.speed, duration: 5 }
                )
                element.emissions = element.vehicle_count * cruiseEmissions.emissions;
             }
             dataObj.push(obj);
        }
    }, this);


    res.json(dataObj);
});

// Route for direct calls to view
app.use("/view-*", mainAuthenticate(), function (req, res) {
    res.sendFile(process.cwd() + "/public/index.html");
});

// Use this route to make the entire app secure.  This forces login for any path in the entire app.
app.use("/", mainAuthenticate(),
    express.static(path.join(__dirname, process.env["base-dir"] ? process.env["base-dir"] : "../public"))
);


// //// error handlers //////

// Development error handler - prints stacktrace
if (config.nodeEnv !== "cloud") {
    app.use(function (err, req, res, next) {
        console.error(err.stack);
        if (res.headersSent) {
            return next(err);
        }
        res.status(err.status || HTTP_INTERNAL_ERROR_STATUS);
        return res.send({
            message: err.message,
            error: err
        });
    });
}

// Production error handler
// No stacktraces leaked to user
app.use(function (err, req, res, next) { // eslint-disable-line no-unused-vars
    if (!res.headersSent) {
        res.status(err.status || HTTP_INTERNAL_ERROR_STATUS);
        res.send({
            message: err.message,
            error: {}
        });
    }
});

module.exports = app;
