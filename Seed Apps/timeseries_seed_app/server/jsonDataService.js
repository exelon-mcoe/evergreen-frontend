const request = require('request');

function getJson(url, method, reqBody) {
    request({
        url: url,
        headers: {
            "content-type": "application/json",
        },
        method: method,
        json: reqBody},
    function (err, response, body) {
        if(!err) {
            return JSON.parse(body);
        }else {
            errObj = {response: response, err: err};
            return JSON.parse(errObj);
        }
    });
}
